# Distributed Spectral Signature Database

A peer to peer Raman spectral signature database.

## Features
- [ ] Storing and accessing substance information (Substance thread)
- [ ] Storing and accessing spectral signatures
- [ ] Fuzzy searching of spectal signatures
- [ ] Fuzzy searching of signature meta-data
- [ ] Trust rating system for peers
- [ ] Confidence rating for spectral signatures
- [ ] Spectrometer calibration validation


## Targets

- Linux (Raspbian, Arch Linux) (Primary)
- A yet unknown unikernel (Secondary)

## Architecture

The application will be based of a Kademlia-like logical network. The network will also support several semantic networks on top of the Kademlia logical address space to enable fuzzy queries for both spectral signatures their meta-data.

The trust system will be based of a central ledger which will be backed by the 


## High level specifications (Work in progress)
### Identity

An identity is a set of public and private keys.

- Creating an identity should require a proof of work
- Public identity should be derived from the private key and verifiable via public key
- Every contributions of an identity should be signed using the private key and verifiable via the public identity

#### Attributes
- Public key
    - Public Idenfier
    - Used to verify ownership of contributions
- Private key
    - Secret key that only the client holding the identity knows
- Calibration score
    - Head of calibration attempts in the ledger?
- Trust score
    - Sum or trust level changes in the ledger?
- Contributions list
    - List of all the contributions from the ledger?

### Contributions

A contribution to the database.

#### Spectral Signature (Contribution)

A new signature, wether it is identified or not.

#### Spectral Signature Review (Contribution)

A signature for an existing signature, wether it is identified or not.

#### Spectral Signature Identification (Contribution)

An identification of a signature, wether it is identified or not.

#### Confidence Level

There are several ways to assign trusts to an identity, we could use a peer review system where a machine could be assigned a trust value. Or we could assign a confidence value in a signature, so if the signature was only matched once, it would come up in search results but have a low base confidence score (perhaps boosted if the associated identity has a high trust score). And if this signature is validated by different identities it would see its score improve every time. Once it reaches a certain thresholds it could get permanently committed to the database (perhaps we could have archive nodes who maintain a copy of every signature above a certain threshold). 